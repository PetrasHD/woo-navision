<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.horiondigital.com
 * @since             1.0.0
 * @package           Woo_Navision
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce and Navision Integration
 * Plugin URI:        http://www.horiondigital.com
 * Description:       WooCommerce ir Navision sistemos integracija. Periodinis likuciu atnaujinimas WooCommerce is Navision sistemos. WooCommerce naujo uzsakymo perdavimas i Navision.
 * Version:           1.0.0
 * Author:            Horion Digital
 * Author URI:        http://www.horiondigital.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woo-navision
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOO_NAVISION_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woo-navision-activator.php
 */
function activate_woo_navision() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-navision-activator.php';
	Woo_Navision_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woo-navision-deactivator.php
 */
function deactivate_woo_navision() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woo-navision-deactivator.php';
	Woo_Navision_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woo_navision' );
register_deactivation_hook( __FILE__, 'deactivate_woo_navision' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woo-navision.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */

function run_woo_navision() {

	$plugin = new Woo_Navision();
	$plugin->run();

}
run_woo_navision();
