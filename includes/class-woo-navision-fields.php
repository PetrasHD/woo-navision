<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.horiondigital.com
 * @since      1.0.0
 *
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 * @author     Horion Digital <info@horiondigital.com>
 */

class Woo_Navision_Fields {

    public function __construct() {
    }

    private function get_fields( $order_id ) {
        $order = wc_get_order( $order_id );
        $items = $order->get_items();
        $shipping = $order->get_shipping_method();
        $order_amount = $order->get_total();

        // $order_date = $order->order_date;
        // $create_date = new DateTime($order_date);
        // $formatted_date = $create_date->format('Y-m-d');

        $array = array(
            // 'orderDate' => $formatted_date,
            'orderAmount' => (float)$order_amount,
            'lines' => array()
        );

        foreach ($items as $item) :

            $product_id = $item['product_id'];
            $product = wc_get_product( $product_id );
            $regular_product_price = $product->get_regular_price();
            $discounted_price = round($item->get_total(), 2);
            $shipping_method = '';

            $sku = $product->get_sku();
            $quantity = $item->get_quantity();
            $discount = ($regular_product_price * $quantity) - $discounted_price;

            if($order->has_shipping_method('local_pickup')):
                $shipping_method = 'VILNIUS';
            else:
                $shipping_method = 'CENTR';
            endif;

            $item_array = array(
                'itemNo' => (string)$sku,
                'locationCode' => (string)$shipping_method,
                'quantity' => (integer)$quantity,
                'discount' => (float)round($discount, 2),
            );

            array_push($array['lines'], $item_array);

        endforeach;

        return $array;
    }

    private function json_generator( $fields_data ) {
        $array_json = json_encode($fields_data, JSON_PRETTY_PRINT);
        print("<pre>".print_r($array_json,true)."</pre>");
    }

    private function send_json_to_navision( $json ) {

    }

    public function fields_run() {
           
        $fields_data = $this->get_fields( 6042 );
        $this->json_generator( $fields_data );
    
        print("<pre>".print_r("Fields run is working",true)."</pre>");
    }

}