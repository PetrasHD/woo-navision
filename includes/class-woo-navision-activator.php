<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.horiondigital.com
 * @since      1.0.0
 *
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 * @author     Horion Digital <info@horiondigital.com>
 */
class Woo_Navision_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		$woo_navision_stock = new Woo_Navision_Stock();
	}

}
