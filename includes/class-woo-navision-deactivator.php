<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.horiondigital.com
 * @since      1.0.0
 *
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 * @author     Horion Digital <info@horiondigital.com>
 */
class Woo_Navision_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		$timestamp = wp_next_scheduled( 'woo_nav_stock_updater' );
		wp_unschedule_event( $timestamp, 'woo_nav_stock_updater' );
	}

}
