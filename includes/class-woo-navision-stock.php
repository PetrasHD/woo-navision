<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.horiondigital.com
 * @since      1.0.0
 *
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Woo_Navision
 * @subpackage Woo_Navision/includes
 * @author     Horion Digital <info@horiondigital.com>
 */

class Woo_Navision_Stock {

    public function __construct() {
    }

    public function schedule_cron() {
        if (!wp_next_scheduled('woo_nav_stock_updater')) {
            wp_schedule_event(time(), 'every_minute', 'woo_nav_stock_updater', null);
        }
    }

    private function get_navision_data() {
        $json ='
        {
            "status": "success",
            "data": [
                {
                    "itemNo": 107347,
                    "description": "...",
                    "quantity": 2,
                    "uom": "VNT",
                    "unitPrice": 50.05,
                    "locationCode": "S08"
                },
                {
                    "itemNo": 107344,
                    "description": "...",
                    "quantity": 0,
                    "uom": "VNT",
                    "unitPrice": 18,
                    "locationCode": "S08"
                }
            ]
        }
        ';
        $navision_data = json_decode($json, true);

        $navision_items = '';

        if($navision_data['status'] === 'success'):
            $navision_items = $navision_data['data'];
        else:
            $navision_items = null;
            print("<pre>".print_r("Nepavyko užmegzti ryšio su serveriu.",true)."</pre>");            
        endif;

        return $navision_items;
    }


    private function stock_updater( $navision_items ) {
        if($navision_items):
            $args = array(
                'post_type' => 'product',
                'posts_per_page' => -1
            );
            $loop = new WP_Query( $args );
        
            while ( $loop->have_posts() ) : $loop->the_post();
        
                $product = get_product($loop->post);
                $product_sku = $product->get_sku();
        
                foreach ($navision_items as $item):
                    if($product_sku == $item['itemNo'] && $item['quantity'] === 0):
                        $product->set_stock_status('outofstock');
                        $product->set_manage_stock( true );
                        $product->set_stock_quantity(0);
                        $product->save();
                        print("<pre>".print_r("Done Out Of Stock.",true)."</pre>");
                    elseif ($product_sku == $item['itemNo'] && $item['quantity'] > 0):
                        $product->set_stock_status('instock');
                        $product->set_manage_stock( true );
                        $product->set_stock_quantity($item['quantity']);
                        $product->save();
                        print("<pre>".print_r("Done In Stock.",true)."</pre>");
                    endif;
                endforeach;
        
            endwhile;
            wp_reset_query();
        else:
            print("<pre>".print_r("Neradau itemu.",true)."</pre>");
        endif;
    }


    public function stock_run() {

        $navision_items = $this->get_navision_data();
        // print("<pre>".print_r($navision_items,true)."</pre>");
    
        $this->stock_updater( $navision_items );
    
        print("<pre>".print_r("Stock run is working.",true)."</pre>");
    }

}