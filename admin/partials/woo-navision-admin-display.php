<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.horiondigital.com
 * @since      1.0.0
 *
 * @package    Woo_Navision
 * @subpackage Woo_Navision/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
